package by.dazerty.androidtaskfive.rest.resp

import com.google.gson.annotations.SerializedName

class BelarusBankInfoBoxResponse {
    @SerializedName("location_name_desc")
    lateinit var locationNameDesc : String
    @SerializedName("gps_x")
    var coordX : Double = 1.0
    @SerializedName("gps_y")
    var coordY : Double = 1.0

    override fun toString(): String {
        return "BBInfoBox(locationNameDesc='$locationNameDesc', coordX=$coordX, coordY=$coordY)"
    }
}