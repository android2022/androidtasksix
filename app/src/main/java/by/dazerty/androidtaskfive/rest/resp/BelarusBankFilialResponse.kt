package by.dazerty.androidtaskfive.rest.resp

import com.google.gson.annotations.SerializedName

class BelarusBankFilialResponse {
    @SerializedName("filial_name")
    lateinit var filialMame : String
    @SerializedName("GPS_X")
    var coordX : Double = 1.0
    @SerializedName("GPS_Y")
    var coordY : Double = 1.0

    override fun toString(): String {
        return "BBFilial(filialMame='$filialMame', coordX=$coordX, coordY=$coordY)"
    }
}