package by.dazerty.androidtaskfive.rest

import by.dazerty.androidtaskfive.rest.resp.BelarusBankAtmResponse
import by.dazerty.androidtaskfive.rest.resp.BelarusBankFilialResponse
import by.dazerty.androidtaskfive.rest.resp.BelarusBankInfoBoxResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface BelarusBankRestAPI {
    @GET("/api/infobox")
    fun getInfoBoxes(@Query("city") city: String) : Observable<ArrayList<BelarusBankInfoBoxResponse>>

    @GET("/api/filials_info")
    fun getFilials(@Query("city") city: String) : Observable<ArrayList<BelarusBankFilialResponse>>

    @GET("/open-banking/v1.0/atms")
    fun getAtms(@Query("city") city: String) : Observable<BelarusBankAtmResponse>
}