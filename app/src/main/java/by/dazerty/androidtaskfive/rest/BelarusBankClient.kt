package by.dazerty.androidtaskfive.rest

import android.util.Log
import by.dazerty.androidtaskfive.rest.resp.BelarusBankAtmResponse
import by.dazerty.androidtaskfive.rest.resp.BelarusBankFilialResponse
import by.dazerty.androidtaskfive.rest.resp.BelarusBankInfoBoxResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class BelarusBankClient {
    private val belarusBankRestAPI: BelarusBankRestAPI

    init {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://belarusbank.by/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()
        belarusBankRestAPI = retrofit.create(BelarusBankRestAPI::class.java)
    }

    fun getAtms(city: String) : Observable<BelarusBankAtmResponse> {
        return belarusBankRestAPI.getAtms(city).doOnError {
            Log.d("BelarusBankClient_LOG", it.localizedMessage)
        }
    }

    fun getInfoboxes(city: String) : Observable<ArrayList<BelarusBankInfoBoxResponse>> {
        return belarusBankRestAPI.getInfoBoxes(city).doOnError {
            Log.d("BelarusBankClient_LOG", it.localizedMessage)
        }
    }

    fun getFilials(city: String) : Observable<ArrayList<BelarusBankFilialResponse>> {
        return belarusBankRestAPI.getFilials(city).doOnError {
            Log.d("BelarusBankClient_LOG", it.localizedMessage)
        }
    }
}