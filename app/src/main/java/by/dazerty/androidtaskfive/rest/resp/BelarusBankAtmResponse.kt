package by.dazerty.androidtaskfive.rest.resp

import by.dazerty.androidtaskfive.models.BelarusBankAtm
import com.google.gson.annotations.SerializedName

class BelarusBankAtmResponse {
    @SerializedName("Data")
    lateinit var data: AtmData

    override fun toString(): String {
        return "$data"
    }
}

class AtmData {
    @SerializedName("ATM")
    lateinit var atms: List<BelarusBankAtm>

    override fun toString(): String {
        return "$atms"
    }
}