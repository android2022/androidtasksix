package by.dazerty.androidtaskfive.models

import by.dazerty.androidtaskfive.gomelCity
import com.google.android.gms.maps.model.LatLng
import kotlin.math.pow
import kotlin.math.sqrt

class BankPoint(
    var type: Type,
    var description : String,
    var position : LatLng
) : Comparable<BankPoint> {
    var distance : Double = 0.0

    init {
        distance = sqrt(
            (gomelCity.latitude - position.latitude).pow(2) +
               (gomelCity.longitude - position.longitude).pow(2)
            )
    }

    override fun toString(): String {
        return "BankPoint(type=$type, description='$description', position=$position, distance=$distance)"
    }

    override fun compareTo(other: BankPoint): Int {
        return distance.compareTo(other.distance)
    }
}

enum class Type {
    INFO_BOX, ATM, FILIAL
}