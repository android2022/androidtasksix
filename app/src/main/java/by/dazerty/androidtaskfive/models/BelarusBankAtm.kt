package by.dazerty.androidtaskfive.models

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName

class BelarusBankAtm(
    private var currency : String = "",
    @SerializedName("Address")
    var address : AtmAddress = AtmAddress()
) {
    override fun toString(): String {
        return "Atm info: address: ${address}, currency = $currency"
    }
}

class AtmAddress(
    var addressLine : String = "",
    @SerializedName("Geolocation")
    var geolocation : AtmGeolocation = AtmGeolocation ()
) {
    override fun toString() : String {
        return "addressLine = $addressLine, place = ${geolocation}"
    }
}

class AtmGeolocation(
    @SerializedName("GeographicCoordinates")
    var coordinates: LatLng = LatLng (1.0, 1.0)
) {
    override fun toString() : String {
        return "(x: ${coordinates.latitude}, y: ${coordinates.longitude})"
    }
}