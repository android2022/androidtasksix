package by.dazerty.androidtaskfive

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import by.dazerty.androidtaskfive.databinding.ActivityMapsBinding
import by.dazerty.androidtaskfive.models.BankPoint
import by.dazerty.androidtaskfive.models.Type
import by.dazerty.androidtaskfive.rest.BelarusBankClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*

const val LOG_TAG = "MapsActivity_LOG"
const val DEFAULT_CITY = "Гомель"
val gomelCity = LatLng(52.425163, 31.015039)

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var mMap : GoogleMap
    private lateinit var binding : ActivityMapsBinding
    private lateinit var bbApi : BelarusBankClient
    private lateinit var loadingDialog : LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadingDialog = LoadingDialog(this)
        bbApi = BelarusBankClient()
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.apply {
            moveCamera(CameraUpdateFactory.newLatLng(gomelCity))
            setMinZoomPreference(12.5f)
        }

        loadAtmMarkers()
    }

    private fun loadAtmMarkers() {
        val atms = bbApi.getAtms(DEFAULT_CITY)
            .flatMap { response ->
                Observable.fromIterable(response.data.atms)
            }
            .map {
                BankPoint(Type.ATM, it.address.addressLine, it.address.geolocation.coordinates)
            }
        val infoboxes = bbApi.getInfoboxes(DEFAULT_CITY)
            .flatMap { response ->
                Observable.fromIterable(response)
            }
            .map {
                BankPoint(Type.INFO_BOX, it.locationNameDesc, LatLng(it.coordX, it.coordY))
            }
        val filials = bbApi.getFilials(DEFAULT_CITY)
            .flatMap { response ->
                Observable.fromIterable(response)
            }
            .map {
                BankPoint(Type.FILIAL, it.filialMame, LatLng(it.coordX, it.coordY))
            }

        val queue = PriorityQueue<BankPoint>()

        Observable.merge(atms, infoboxes, filials)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                loadingDialog.show()
            }
            .doAfterTerminate {
                loadingDialog.hide()
            }
            .subscribe(
                {
                    queue.add(it)
                },
                {
                    Log.e(LOG_TAG, it.toString())
                    val alertDialog = AlertDialog.Builder(this)
                    alertDialog.apply {
                        setTitle(getString(R.string.error_title))
                        setMessage(getString(R.string.error_message))
                        setPositiveButton(android.R.string.ok
                        ) { dialog, _ -> dialog?.cancel() }
                        show()
                    }
                },
                {
                    Log.d(LOG_TAG, "Finish loading")

                    var count = 11
                    while (!queue.isEmpty() && count-- > 0) {
                        val point : BankPoint? = queue.poll()
                        point?.let {
                            mMap.addMarker(
                                MarkerOptions()
                                    .position(point.position)
                                    .icon(
                                        when (point.type) {
                                            Type.ATM -> BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)
                                            Type.FILIAL -> BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)
                                            Type.INFO_BOX -> BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)
                                        })
                                    .title(getString(R.string.point_info, point.type, point.description))
                            )
                        }
                    }
                }
            )
    }
}